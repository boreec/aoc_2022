use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};

/// Structure to represent a command to move
/// crates around the different stacks.
struct Command {
    stack_src: usize,
    stack_dst: usize,
    quantity: usize,
}

fn main() {
    
    // open the file
    let file = File::open("input.txt")
        .expect("failed to open input.txt");    
    
    // retrieve information from the file
    let (mut stacks, commands) = parse_input(file);
    
    // run the commands
    for c in commands {
        
        let stack_src_size = stacks
            .get(&c.stack_src)
            .expect("failed to retrieve stack to unload")
            .len();
        
        let crates = stacks
            .get_mut(&c.stack_src)
            .expect("failed to retrieve stack to unload")
            .split_off(stack_src_size - c.quantity);
        
        stacks
            .get_mut(&c.stack_dst)
            .expect("failed to retrieve stack to load")
            .extend(crates);
    }
    
    // print the result
    for i in 1..=stacks.len() {
        print!("{}", stacks.get_mut(&i).expect("").pop().unwrap());
    }
    println!("");
}

/// Parse the input from a file "input.txt" and
/// return a HashMap for the crates (stack id and vec of char)
/// and a vector of commands.
fn parse_input(file: std::fs::File) -> (HashMap<usize, Vec<char>>, Vec<Command>) {
    let reader = BufReader::new(file);    
    let mut stacks: HashMap<usize, Vec<char>> = HashMap::new();
    let mut commands = vec![];
    for line in reader.lines() {
        match line {
            Ok(l) => {
                // crates
                if l.contains("[") {
                    for i in 0..l.len() {
                        let current_char = l.chars().nth(i).unwrap();
                        if current_char.is_ascii_uppercase() {
                            let stack_num = (i / 4) + 1;
                            if let Some(x) = stacks.get_mut(&stack_num) {
                                x.insert(0, current_char);
                            } else {
                                stacks.insert(stack_num, vec![current_char]);
                            }
                        }    
                    }
                }
                // commands
                if l.contains("move") {
                    commands.push(get_command(&l));
                }
            }
            Err(_) => {
                panic!("failed to parse input");
            }
        }
    }
    (stacks, commands)
}

/// Create a `Command` object from a line containing
/// a command instruction like `move x from y to z`
fn get_command(line: &str) -> Command {
    let words: Vec<_> = line.split_ascii_whitespace().collect();
    
    let quantity = words[1]
        .parse::<usize>()
        .expect("failed to retrieve crates quantity in command line!");
    
    let stack_src = words[3]
        .parse::<usize>()
        .expect("failed to retrieve stack src in command line");
    
    let stack_dst = words[5]
        .parse::<usize>()
        .expect("failed to retrieve stack dst in command line");
    
    Command {
        stack_src,
        stack_dst,
        quantity,    
    }
}