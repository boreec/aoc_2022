use std::fs::File;
use std::io::{BufRead, BufReader};

/// Represents one of the 3 possible hand move
/// occuring in a Rock-Paper-Scissors game.
#[derive(Clone, Copy)]
enum HandMove {
    Rock,
    Paper,
    Scissors,    
}

impl HandMove {
    
    /// Create a new HandMove from a string slice
    /// corresponding to the HandMove's character.
    fn new(c: &str) -> HandMove {
        match c {
            "A" | "X" => HandMove::Rock,
            "B" | "Y" => HandMove::Paper,
            "C" | "Z" => HandMove::Scissors,
            _ => {
                panic!("no handmove for {}", c);
            }
        }
    }
    
    /// Give the round points depending on the
    /// two players hand moves.
    fn round(p1: HandMove, p2: HandMove) -> i32 {
        match p1 {
            HandMove::Rock => {
                match p2 {
                    HandMove::Rock => 1 + 3,
                    HandMove::Paper => 1 + 0,
                    HandMove::Scissors => 1 + 6,
                }
            }
            HandMove::Paper => {
                match p2 {
                    HandMove::Rock => 2 + 6,
                    HandMove::Paper => 2 + 3,
                    HandMove::Scissors => 2 + 0,
                }
            }
            HandMove::Scissors => {
                match p2 {
                    HandMove::Rock => 3 + 0,
                    HandMove::Paper => 3 + 6,
                    HandMove::Scissors => 3 + 3,
                }
            }
        }
    }
    
    /// Return the move to play according to the
    /// game result provided.
    fn move_to_play(p1: HandMove, gr: GameResult) -> HandMove {
        match p1 {
            HandMove::Rock => {
                match gr {
                    GameResult::Win => HandMove::Paper,
                    GameResult::Draw => HandMove::Rock,
                    GameResult::Lose => HandMove::Scissors,
                }
            }
            HandMove::Paper => {
                match gr {
                    GameResult::Win => HandMove::Scissors,
                    GameResult::Draw => HandMove::Paper,
                    GameResult::Lose => HandMove::Rock,
                }
            }
            HandMove::Scissors => {
                match gr {
                    GameResult::Win => HandMove::Rock,
                    GameResult::Draw => HandMove::Scissors,
                    GameResult::Lose => HandMove::Paper,
                }
            }
        }
    }
}

/// Represent one of the 3 possible game result
/// occuring after a game of Rock-Paper-Scissors.
enum GameResult {
    Win,
    Draw,
    Lose,
}

impl GameResult{
    
    /// Create a new GameResult object from a string
    /// slice representing the desired GameResult.
    fn new(c: &str) -> GameResult {
        match c {
            "X" => GameResult::Lose,
            "Y" => GameResult::Draw,
            "Z" => GameResult::Win,
            _ => { panic!("no game result for {}!", c) }
        }
    }
}

fn main() {
    let file = File::open("input.txt")
        .expect("failed to open input.txt");
    
    let reader = BufReader::new(file);
    
    let mut strategy_score = 0;
    for line in reader.lines() {
        match line {
            Ok(s) => {
                // Check if the current line contains the moves.
                if s.len() >= 3 {
                    // Retrieve the move for each player.
                    let op_move_char = &s[0..1]; // Slice retrieval is only possible
                    let my_move_char = &s[2..3]; // because we know the format.
                    
                    let op_move = HandMove::new(op_move_char);
                    let my_move = HandMove::move_to_play(op_move, GameResult::new(my_move_char));
                    
                    let round_score = HandMove::round(my_move, op_move);
                    
                    strategy_score += round_score;
                }
            }
            Err(_) => {
                panic!("failed to read line!");
            }
        }
    }
    
    println!("strategy score: {}", strategy_score);
}
