use std::fs;

fn main() {
    let text = fs::read_to_string("input.txt")
        .expect("failed to read input into string");
    
    println!("characters to process for puzzle #1: {}", get_marker(&text, 4));
    println!("characters to process for puzzle #2: {}", get_marker(&text, 14));
}

/// Get the index corresponding to the characters that need to be
/// processed in order to find a sequence of unique characters of 
/// size `marker_length` in `datastream`.
fn get_marker(datastream: &str, marker_length: usize) -> usize {
    let mut i = 0;
    
    // Loop over the datastream and look at each position
    // if the substring of size marker_length is unique.
    while i < datastream.len() - marker_length {
        if is_unique(&datastream[i..i+marker_length]) {
            return i + marker_length;
        }
        i = i + 1;
    }
    i
}

/// Return true if a string contains only unique characters,
/// false otherwise.
fn is_unique(datastream: &str) -> bool {
    // Store visited characters in a buffer.
    let mut visited_chars: Vec<char> = vec![];
    
    let mut i = 0;
    
    // Loop until the end is reached or an already visited
    // characters appear. 
    while i < datastream.len() {
        let current_char = datastream.chars().nth(i).unwrap();
        if visited_chars.contains(&current_char) {
            return false;
        } 
        visited_chars.push(current_char);
        i = i + 1;
    }
    true
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn test_get_marker_1_4() {
        assert_eq!(get_marker("mjqjpqmgbljsphdztnvjfqwrcgsmlb", 4), 7);
    }
    
    #[test]
    fn test_get_marker_1_14() {
        assert_eq!(get_marker("mjqjpqmgbljsphdztnvjfqwrcgsmlb", 14), 19);
    }
    
    #[test]
    fn test_get_marker_2_4() {
        assert_eq!(get_marker("bvwbjplbgvbhsrlpgdmjqwftvncz", 4), 5);
    }
    
    #[test]
    fn test_get_marker_2_14() {
        assert_eq!(get_marker("bvwbjplbgvbhsrlpgdmjqwftvncz", 14), 23);
    }
    
    #[test]
    fn test_get_marker_3_4() {
        assert_eq!(get_marker("nppdvjthqldpwncqszvftbrmjlhg", 4), 6);
    }
    
    #[test]
    fn test_get_marker_3_14() {
        assert_eq!(get_marker("nppdvjthqldpwncqszvftbrmjlhg", 14), 23);
    }
    
    #[test]
    fn test_get_marker_4_4() {
        assert_eq!(get_marker("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", 4), 10);
    }
    
    #[test]
    fn test_get_marker_4_14() {
        assert_eq!(get_marker("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", 14), 29);
    }
    
    #[test]
    fn test_get_marker_5_4() {
        assert_eq!(get_marker("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", 4), 11);
    }
    
    #[test]
    fn test_get_marker_5_14() {
        assert_eq!(get_marker("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", 14), 26);
    }
    
    #[test]
    fn test_is_unique_1() {
        assert!(is_unique("abcd"));
        assert!(is_unique("LzKs"));
        assert!(!is_unique("abca"));
    }
}
