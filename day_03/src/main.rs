use std::collections::HashSet;
use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let file = File::open("input.txt")
        .expect("failed to open input.txt");
    
    let reader = BufReader::new(file);
    
    // Use sets to store unique items carried by each elf.
    let mut elf1 = HashSet::new();
    let mut elf2 = HashSet::new();
    let mut elf3 = HashSet::new();
    
    let mut priority_sum = 0;
    for line in reader.lines() {
        match line {
            Ok(rucksack) => {
                // Fill 1st elf's set.
                if elf1.is_empty() {
                    elf1 = rucksack.chars().collect();
                } 
                // Fill 2nd elf's set.
                else if elf2.is_empty() {
                    elf2 = rucksack.chars().collect();
                }
                // Fill 3rd elf's set
                else if elf3.is_empty() {
                    elf3 = rucksack.chars().collect();
                    
                    // Intersect all sets to find duplicate item.
                    let d: Vec<_> = elf1
                        .iter()
                        .filter(|k| elf2.contains(k))
                        .filter(|j| elf3.contains(j))
                        .collect();
                    
                    // Only one item should be found.
                    if d.len() != 1 {
                        panic!("badge not found!");
                    }
                    
                    priority_sum += get_item_priority(*d[0]);
                    
                    // Reset the content for all sets.
                    elf1 = HashSet::new();
                    elf2 = HashSet::new();
                    elf3 = HashSet::new();
                }
            }
            Err(_) => {
                panic!("failed to read line!");
            }
        }
    }
    
    println!("priority sum: {priority_sum}");
}

/// Compute the priority for a given item represented by 
/// the character `c`. If the given character is not a 
/// recognized item, the returned priority is 0.
fn get_item_priority(c: char) -> i32 {
    if c >= 'a' && c <= 'z' {
        return c as i32 - 'a' as i32 + 1;
    }
    else if c >= 'A' && c <= 'Z' {
        return c as i32 - 'A' as i32 + 27;
    }
    0
}