use rctree::Node;

use std::fs::File;
use std::io::{BufRead, BufReader};

/// Represents the kind of a file in the given filesystem
#[derive(Debug, Eq, PartialEq)]
enum FileKind {
    Regular,
    Directory,
}

/// Contains all information related to a file on the device.
#[derive(Debug, Eq, PartialEq)]
struct DeviceFile {
    kind: FileKind,
    size: usize,
    name: String,
}

impl DeviceFile {
    fn new(kind: FileKind, size: usize, name: &str) -> DeviceFile {
        DeviceFile {
            kind,
            size,
            name: name.to_owned(),
        }
    }
}

impl From<&String> for DeviceFile {
    /// Build a DeviceFile object from a provided line of text.
    fn from(line: &String) -> Self {
        let words: Vec<_> = line.split_whitespace().collect();

        if words[0] == "dir" {
            DeviceFile {
                kind: FileKind::Directory,
                size: 0,
                name: words[1].to_string(),
            }
        } else {
            DeviceFile {
                kind: FileKind::Regular,
                size: words[0].parse::<usize>().unwrap(),
                name: words[1].to_string(),
            }
        }
    }
}

fn main() {
    // Open file.
    let file = File::open("input.txt").expect("failed to open input.txt");

    // Parse file and build a tree from it.
    let root_node = build_tree(file);

    // Update the size for each directory.
    update_tree(root_node.clone());

    // Print the tree to make sure its structure.
    // print_tree(root_node.clone(), 0);

    println!("problem #1: {}", solve_problem_1(root_node.clone()));

    let required_space = root_node.borrow().size - (70_000_000 - 30_000_000);
    let mut deletable_dirs: Vec<usize> = Vec::new();
    solve_problem_2(root_node, required_space, &mut deletable_dirs);
    deletable_dirs.sort();
    println!("problem #2: {}", &deletable_dirs[0]);
}

fn build_tree(file: std::fs::File) -> Node<DeviceFile> {
    let reader = BufReader::new(file);

    let root_dir = DeviceFile::new(FileKind::Directory, 0, "/");

    let root_node = Node::new(root_dir);
    let mut current_node = root_node.clone();

    for line in reader.lines() {
        match line {
            Ok(l) => {
                if l.len() <= 2 {
                    continue;
                }

                if l.contains("$ cd") {
                    let cd_target = get_cd_target(&l);

                    // Set current_node to its parent.
                    if cd_target == *".." {
                        if current_node != root_node {
                            current_node = current_node.clone().parent().unwrap();
                        }
                    }
                    // Set current_node to one of its child.
                    else {
                        for c in current_node.clone().children() {
                            if c.borrow().name == cd_target {
                                current_node = c.clone();
                                break;
                            }
                        }
                    }
                } else if !l.contains("$ ls") {
                    let new_file = DeviceFile::from(&l);
                    // add new file to the current node children
                    current_node.append(Node::new(new_file));
                }
            }
            Err(_) => {
                panic!("failed to read an input line");
            }
        }
    }

    root_node
}

/// Solve the 1st problem of day 07
/// The result of this function corresponds to the sum
/// of the directories whose size is at most 100000.
///
/// Note: This function is recursive, and so the initial
/// call must provide the root node.
fn solve_problem_1(node: Node<DeviceFile>) -> usize {
    let memory_limit = 100000;

    // Ignore everything that is not a directory.
    if node.borrow().kind != FileKind::Directory {
        return 0;
    }

    if node.borrow().size <= memory_limit {
        return node
            .children()
            .filter(|x| x.borrow().kind == FileKind::Directory)
            .fold(0, |acc, x| acc + x.borrow().size)
            + node.borrow().size;
    }

    node.children().fold(0, |acc, x| acc + solve_problem_1(x))
}

/// Solve the 2nd problem of day 07
/// This function returns nothing, but it fills a given vector `candidates`
/// with all the directories whose deletion could satisfy the update
/// requirement.
///
/// Note: This function is recursive, and so the initial call must
/// provide the root node, the required space and an empty vector.
fn solve_problem_2(node: Node<DeviceFile>, required_space: usize, candidates: &mut Vec<usize>) {
    if node.borrow().kind != FileKind::Directory {
        return;
    }

    if node.borrow().size < required_space {
        return;
    }

    candidates.push(node.borrow().size);

    node.children()
        .for_each(|x| solve_problem_2(x, required_space, candidates));
}

/// For each directory in the three, calculate its size (which
/// is the sum of its children' size) and store it.
fn update_tree(node: Node<DeviceFile>) {
    // don't update the current file if it's not a directory
    if node.borrow().kind != FileKind::Directory {
        return;
    }

    node.children().for_each(update_tree);

    node.borrow_mut().size = node.children().fold(0, |acc, x| acc + x.borrow().size);
}

/// Display a tree with each level indented according to its
/// depths. Thus, initial call should set `indent` to 0.
fn print_tree(node: Node<DeviceFile>, indent: usize) {
    let node_data = node.borrow();

    print!("{:-<1$}", "", indent);

    match node_data.kind {
        FileKind::Regular => {
            println!("{} ({})", node_data.name, node_data.size);
        }
        FileKind::Directory => {
            if node_data.name != "/" {
                println!("{}/ ({})", node_data.name, node_data.size);
            } else {
                println!("{} ({})", node_data.name, node_data.size);
            }
        }
    }

    node.children().for_each(|x| print_tree(x, indent + 1));
}

fn get_cd_target(line: &str) -> String {
    let words: Vec<_> = line.split_whitespace().collect();

    words[2].to_string()
}

#[cfg(test)]
mod tests {

    use super::*;

    /// Create a tree witht the same structure as provided in
    /// the problem's description.
    fn mock_tree() -> Node<DeviceFile> {
        let root = DeviceFile::new(FileKind::Directory, 0, "/");
        let a = DeviceFile::new(FileKind::Directory, 0, "a");
        let e = DeviceFile::new(FileKind::Directory, 0, "e");
        let i = DeviceFile::new(FileKind::Regular, 584, "i");
        let f = DeviceFile::new(FileKind::Regular, 29116, "f");
        let g = DeviceFile::new(FileKind::Regular, 2557, "g");
        let h_lst = DeviceFile::new(FileKind::Regular, 62596, "h.lst");
        let b_txt = DeviceFile::new(FileKind::Regular, 14848514, "b.txt");
        let c_dat = DeviceFile::new(FileKind::Regular, 8504156, "c.dat");
        let d = DeviceFile::new(FileKind::Directory, 0, "d");
        let j = DeviceFile::new(FileKind::Regular, 4060174, "j");
        let d_log = DeviceFile::new(FileKind::Regular, 8033020, "d.log");
        let d_ext = DeviceFile::new(FileKind::Regular, 5626152, "d.ext");
        let k = DeviceFile::new(FileKind::Regular, 7214296, "k");

        let root_node = Node::new(root);
        let a_node = Node::new(a);
        let e_node = Node::new(e);
        let i_node = Node::new(i);
        let f_node = Node::new(f);
        let g_node = Node::new(g);
        let h_lst_node = Node::new(h_lst);
        let b_txt_node = Node::new(b_txt);
        let c_dat_node = Node::new(c_dat);
        let d_node = Node::new(d);
        let j_node = Node::new(j);
        let d_log_node = Node::new(d_log);
        let d_ext_node = Node::new(d_ext);
        let k = Node::new(k);

        e_node.append(i_node.clone());
        a_node.append(e_node.clone());
        a_node.append(f_node.clone());
        a_node.append(g_node.clone());
        a_node.append(h_lst_node.clone());

        d_node.append(j_node.clone());
        d_node.append(d_log_node.clone());
        d_node.append(d_ext_node.clone());
        d_node.append(k.clone());

        root_node.append(a_node.clone());
        root_node.append(b_txt_node.clone());
        root_node.append(c_dat_node.clone());
        root_node.append(d_node.clone());

        return root_node.clone();
    }

    #[test]
    fn test_problem_1() {
        let root = mock_tree();
        update_tree(root.clone());
        assert_eq!(solve_problem_1(root.clone()), 95437);
    }

    #[test]
    fn test_problem_2() {
        let root = mock_tree();
        update_tree(root.clone());
        let mut deletable_dir: Vec<usize> = vec![];
        solve_problem_2(root.clone(), 8381165, &mut deletable_dir);
        deletable_dir.sort();
        assert_eq!(deletable_dir[0], 24933642);
    }
}
