use std::collections::HashSet;
use std::fs::File;
use std::io::{BufRead, BufReader};

#[derive(Debug)]
enum Direction {
    Left,
    Right,
    Up,
    Down,
}

impl From<&str> for Direction {
    fn from(s: &str) -> Self {
        match s {
            "L" => Direction::Left,
            "R" => Direction::Right,
            "U" => Direction::Up,
            "D" => Direction::Down,
            _ => {
                panic!("unknown direction");
            }
        }
    }
}

struct Rope {
    body: Vec<(i32, i32)>,
    visited: HashSet<(i32, i32)>,
}

impl Rope {
    fn new(size: usize) -> Rope {
        assert!(size >= 2);
        Rope {
            body: vec![(0, 0); size],
            visited: HashSet::new(),
        }
    }

    fn apply_commands(&mut self, commands: &Vec<Command>) {
        for c in commands {
            for _ in 0..c.step {
                self.move_toward(&c.direction);
            }
        }
    }

    fn move_toward(&mut self, direction: &Direction) {
        match direction {
            Direction::Left => {
                self.move_left();
            }
            Direction::Right => {
                self.move_right();
            }
            Direction::Up => {
                self.move_up();
            }
            Direction::Down => {
                self.move_down();
            }
        }

        // Make sure that all knots are connected after the move.
        for i in 1..self.body.len() {
            assert!(self.are_knots_connected(self.body[i], self.body[i - 1]));
        }

        // Insert the last knot coordinates into the visited ones.
        self.visited.insert(self.body[self.body.len() - 1]);
    }

    fn move_left(&mut self) {
        // Move the head with no constraints.
        self.body[0].0 -= 1;

        // For the next knots, move them only if they
        // are not connected to the previous one.
        for i in 1..self.body.len() {
            if !self.are_knots_connected(self.body[i - 1], self.body[i]) {
                self.body[i].0 -= 1;

                if self.body[i].1 < self.body[i - 1].1 {
                    self.body[i].1 += 1;
                } else if self.body[i].1 > self.body[i - 1].1 {
                    self.body[i].1 -= 1;
                }
            }
        }
    }

    fn move_right(&mut self) {
        // Move the head with no constraints.
        self.body[0].0 += 1;

        // For the next knots, move them only if they
        // are not connected to the previous one.
        for i in 1..self.body.len() {
            if !self.are_knots_connected(self.body[i], self.body[i - 1]) {
                self.body[i].0 += 1;

                if self.body[i].1 < self.body[i - 1].1 {
                    self.body[i].1 += 1;
                } else if self.body[i].1 > self.body[i - 1].1 {
                    self.body[i].1 -= 1;
                }
            }
        }
    }

    fn move_up(&mut self) {
        // Move the head with no constraints.
        self.body[0].1 -= 1;

        // For the next knots, move them only if they
        // are not connected to the previous one.
        for i in 1..self.body.len() {
            if !self.are_knots_connected(self.body[i], self.body[i - 1]) {
                self.body[i].1 -= 1;

                if self.body[i].0 > self.body[i - 1].0 {
                    self.body[i].0 -= 1;
                } else if self.body[i].0 < self.body[i - 1].0 {
                    self.body[i].0 += 1;
                }
            }
        }
    }

    fn move_down(&mut self) {
        // Move the head with no constraints.
        self.body[0].1 += 1;

        // For the next knots, move them only if they
        // are not connected to the previous one.
        for i in 1..self.body.len() {
            if !self.are_knots_connected(self.body[i], self.body[i - 1]) {
                self.body[i].1 += 1;

                if self.body[i].0 > self.body[i - 1].0 {
                    self.body[i].0 -= 1;
                } else if self.body[i].0 < self.body[i - 1].0 {
                    self.body[i].0 += 1;
                }
            }
        }
    }

    fn are_knots_diagonally_connected(&self, k1: (i32, i32), k2: (i32, i32)) -> bool {
        return (k1.0 == k2.0 - 1 && (k1.1 == k2.1 - 1 || k1.1 == k2.1 + 1))
            || (k1.0 == k2.0 + 1 && (k1.1 == k2.1 - 1 || k1.1 == k2.1 + 1));
    }

    fn are_knots_overlapping(&self, k1: (i32, i32), k2: (i32, i32)) -> bool {
        return k1 == k2;
    }

    fn are_knots_connected(&self, k1: (i32, i32), k2: (i32, i32)) -> bool {
        self.are_knots_diagonally_connected(k1, k2)
            || self.are_knots_overlapping(k1, k2)
            || (k1.0 == k2.0 && k1.1 == k2.1 - 1)
            || (k1.0 == k2.0 && k1.1 == k2.1 + 1)
            || (k1.1 == k2.1 && k1.0 == k2.0 - 1)
            || (k1.1 == k2.1 && k1.0 == k2.0 + 1)
    }
}

#[derive(Debug)]
struct Command {
    step: i32,
    direction: Direction,
}

impl Command {
    fn new(step: i32, direction: Direction) -> Command {
        Command { step, direction }
    }
}

impl From<String> for Command {
    fn from(file: String) -> Self {
        let words: Vec<_> = file.split_whitespace().collect();
        let steps = words[1].parse::<i32>().unwrap();
        Command::new(steps, Direction::from(words[0]))
    }
}

fn main() {
    solve_problem_1();
    solve_problem_2();
}

fn solve_problem_1() {
    let commands = parse_input("input.txt");
    let mut rope = Rope::new(2);

    rope.apply_commands(&commands);
    println!("problem #1: {}", rope.visited.len());
}

fn solve_problem_2() {
    let commands = parse_input("input.txt");
    let mut rope = Rope::new(10);

    rope.apply_commands(&commands);
    println!("problem #2: {}", rope.visited.len());
}

fn parse_input(filename: &str) -> Vec<Command> {
    let file = File::open(filename).expect("failed to open file.");
    let reader = BufReader::new(file);

    let mut commands = vec![];

    for line in reader.lines() {
        match line {
            Ok(l) => {
                if l.len() < 2 {
                    continue;
                }
                commands.push(Command::from(l));
            }
            Err(_) => {
                panic!("failed to read input line");
            }
        }
    }
    commands
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_rope_1_example_input() {
        let commands = parse_input("example.txt");
        let mut rope = Rope::new(2);
        rope.apply_commands(&commands);
        assert_eq!(rope.visited.len(), 13);
    }

    #[test]
    fn test_rope_9_example_input() {
        let commands = parse_input("example.txt");
        let mut rope = Rope::new(10);
        rope.apply_commands(&commands);
        assert_eq!(rope.visited.len(), 1);
    }

    #[test]
    fn test_rope_9_larger_example_input() {
        let commands = parse_input("larger_example.txt");
        let mut rope = Rope::new(10);
        rope.apply_commands(&commands);
        assert_eq!(rope.body[0], (-11, -15));
        assert_eq!(rope.body[1], (-11, -14));
        assert_eq!(rope.body[2], (-11, -13));
        assert_eq!(rope.body[3], (-11, -12));
        assert_eq!(rope.body[4], (-11, -11));
        assert_eq!(rope.body[5], (-11, -10));
        assert_eq!(rope.body[6], (-11, -9));
        assert_eq!(rope.body[7], (-11, -8));
        assert_eq!(rope.body[8], (-11, -7));
        assert_eq!(rope.body[9], (-11, -6));
        assert_eq!(rope.visited.len(), 36);
    }

    #[test]
    fn test_rope_1_move_right() {
        let commands = vec![Command::new(5, Direction::Right)];
        let mut rope = Rope::new(2);
        rope.apply_commands(&commands);
        assert_eq!(rope.visited.len(), 5);
        assert_eq!(rope.body[0], (5, 0));
        assert_eq!(rope.body[1], (4, 0));
    }

    #[test]
    fn test_rope_1_move_left() {
        let commands = vec![Command::new(5, Direction::Left)];
        let mut rope = Rope::new(2);
        rope.apply_commands(&commands);
        assert_eq!(rope.visited.len(), 5);
        assert_eq!(rope.body[0], (-5, 0));
        assert_eq!(rope.body[1], (-4, 0));
    }

    #[test]
    fn test_rope_1_move_up() {
        let commands = vec![Command::new(5, Direction::Up)];
        let mut rope = Rope::new(2);
        rope.apply_commands(&commands);
        assert_eq!(rope.visited.len(), 5);
        assert_eq!(rope.body[0], (0, -5));
        assert_eq!(rope.body[1], (0, -4));
    }
    #[test]
    fn test_rope_1_move_down() {
        let commands = vec![Command::new(5, Direction::Down)];
        let mut rope = Rope::new(2);
        rope.apply_commands(&commands);
        assert_eq!(rope.visited.len(), 5);
        assert_eq!(rope.body[0], (0, 5));
        assert_eq!(rope.body[1], (0, 4));
    }

    #[test]
    fn test_rope_1_move_right_and_left() {
        let commands = vec![
            Command::new(5, Direction::Right),
            Command::new(5, Direction::Left),
        ];
        let mut rope = Rope::new(2);
        rope.apply_commands(&commands);
        assert_eq!(rope.visited.len(), 5);
        assert_eq!(rope.body[0], (0, 0));
        assert_eq!(rope.body[1], (1, 0));
    }

    #[test]
    fn test_rope_1_move_up_and_down() {
        let commands = vec![
            Command::new(5, Direction::Up),
            Command::new(5, Direction::Down),
        ];
        let mut rope = Rope::new(2);
        rope.apply_commands(&commands);
        assert_eq!(rope.visited.len(), 5);
        assert_eq!(rope.body[0], (0, 0));
        assert_eq!(rope.body[1], (0, -1));
    }

    #[test]
    fn test_rope_1_move_left_and_up() {
        let mut commands = vec![];
        for _ in 0..5 {
            commands.push(Command::new(1, Direction::Left));
            commands.push(Command::new(1, Direction::Up));
        }
        let mut rope = Rope::new(2);
        rope.apply_commands(&commands);
        assert_eq!(rope.body[0], (-5, -5));
        assert_eq!(rope.body[1], (-4, -4));
        assert_eq!(rope.visited.len(), 5);
    }
}
