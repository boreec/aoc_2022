use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let file = File::open("input.txt")
        .expect("failed to open input.txt");
    
    let reader = BufReader::new(file);
    
    let mut overlap_quantity = 0;
    for line in reader.lines() {
        match line {
            Ok(s) => {
                // Ignore blank lines in the input file.
                if s.len() <= 2 {
                    continue;
                }
                
                // Retrieve the sections for each pair.
                let (elf_i, elf_j) = parse_line(&s);
                
                // Check the overlap
                if  (elf_i.0 >= elf_j.0 && elf_i.0 <= elf_j.1) ||
                    (elf_i.1 >= elf_j.0 && elf_i.1 <= elf_j.1) ||
                    (elf_j.0 >= elf_i.0 && elf_j.0 <= elf_i.1) ||
                    (elf_j.1 >= elf_i.0 && elf_j.1 <= elf_i.1) {
                    overlap_quantity += 1;
                }
            }
            Err(_) => {
                panic!("failed to read line");
            }
        }
    }
    
    println!("overlap: {overlap_quantity}");
}

/// Parse the file and return a tuple of 2 tuples
/// corresponding to the beginning and the ending
/// sections for each elf.
fn parse_line(s: &str) -> ((i32, i32), (i32, i32)){
    let hyp_1 = s
        .find('-')
        .expect("error in the input, no 1st hyphen");
    let hyp_2 = s
        .rfind('-')
        .expect("error in the input, no 2nd hyphen");
    let comma = s
        .find(',')
        .expect("error in the input, no comma");
    (
        (
            s[..hyp_1].parse::<i32>().unwrap(),
            s[hyp_1 + 1..comma].parse::<i32>().unwrap()
        ), 
        (
            s[comma + 1..hyp_2].parse::<i32>().unwrap(),
            s[hyp_2 + 1..].parse::<i32>().unwrap()
        )
    )
}