use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {    
    let file = File::open("input.txt")
        .expect("failed to open input.txt");
    
    let reader = BufReader::new(file);
    
    // use array to store the 3 elves carrying the items with the most calories.
    let mut top_3_calories: [i32; 3] = [0, 0, 0];
    
    // save the calories for the current elf
    let mut current_elf_calories = 0;
    
    for line in reader.lines() {
        match line {
            Ok(s) => {
                // try to parse the line into a number
                match s.parse::<i32>() {
                    Ok(i) => {
                        current_elf_calories += i;
                    }
                    // no number means empty line, and we move on the next elf
                    Err(_) => {
                        if current_elf_calories > 0 {
                            if current_elf_calories > top_3_calories[0] {
                                top_3_calories[2] = top_3_calories[1];
                                top_3_calories[1] = top_3_calories[0];
                                top_3_calories[0] = current_elf_calories;    
                            }
                            else if current_elf_calories > top_3_calories[1] {
                                top_3_calories[2] = top_3_calories[1];
                                top_3_calories[1] = current_elf_calories;
                            }
                            else if current_elf_calories > top_3_calories[2] {
                                top_3_calories[2] = current_elf_calories;
                            }
                        }                        
                        current_elf_calories = 0;
                    }
                }
            }
            Err(_) => {
                panic!("failed to read line");
            }
        }
    }
    println!("total of the 3 elves with the most calories: {}", 
        top_3_calories[0] + top_3_calories[1] + top_3_calories[2]
    )
}
