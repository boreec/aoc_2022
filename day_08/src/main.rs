use std::fs::File;
use std::io::{BufRead, BufReader};

/// Represent a tree patch, or forest as explained
/// in the problem description.
struct TreePatch {
    size: usize,
    grid: Vec<usize>,
}

impl TreePatch {
    /// Create a new TreePatch object with a given `size`
    /// (which corresponds to the size of one side of the forest)
    /// and a given `grid` (vector containing the height of every
    /// tree in that forest).
    fn new(size: usize, grid: Vec<usize>) -> TreePatch {
        TreePatch { size, grid }
    }

    /// Compute the number of visible trees in the forest.
    /// In other words, the number of trees that can be
    /// seen from outside the forest.
    fn visible_trees(&self) -> usize {
        (0..self.grid.len()).filter(|x| self.is_visible(*x)).count()
    }

    /// Check if a tree is visible from outside the forest.
    fn is_visible(&self, index: usize) -> bool {
        self.is_visible_from_left(index)
            || self.is_visible_from_right(index)
            || self.is_visible_from_top(index)
            || self.is_visible_from_bottom(index)
    }

    /// Check if an index (corresponding to a tree), can be
    /// seen from the left of the forest. If the tree is on
    /// the left edge of the forest, it is visible.
    fn is_visible_from_left(&self, index: usize) -> bool {
        if index % self.size == 0 {
            return true;
        }

        let mut x = (index % self.size) - 1;
        let y = index / self.size;
        let tree_height = self.grid[index];
        while x > 0 {
            if self.grid[y * self.size + x] >= tree_height {
                return false;
            }
            x -= 1;
        }
        self.grid[y * self.size + x] < tree_height
    }

    /// Check if an index (corresponding to a tree), can be
    /// seen from the right of the forest. If the tree is on
    /// the right edge of the forest, it is visible.
    fn is_visible_from_right(&self, index: usize) -> bool {
        if (index + 1) % self.size == 0 {
            return true;
        }
        let mut x = (index % self.size) + 1;
        let y = index / self.size;
        let tree_height = self.grid[index];
        while x < self.size {
            if self.grid[y * self.size + x] >= tree_height {
                return false;
            }
            x += 1;
        }
        true
    }

    /// Check if an index (corresponding to a tree), can be
    /// seen from the top of the forest. If the tree is on
    /// the top edge of the forest, it is visible.
    fn is_visible_from_top(&self, index: usize) -> bool {
        if index < self.size {
            return true;
        }

        let x = index % self.size;
        let mut y = (index / self.size) - 1;
        let tree_height = self.grid[index];
        while y > 0 {
            if self.grid[y * self.size + x] >= tree_height {
                return false;
            }
            y -= 1;
        }
        self.grid[y * self.size + x] < tree_height
    }

    /// Check if an index (corresponding to a tree), can be
    /// seen from the bottom of the forest. If the tree is on
    /// the bottom edge of the forest, it is visible.
    fn is_visible_from_bottom(&self, index: usize) -> bool {
        if index >= self.size * (self.size - 1) {
            return true;
        }

        let x = index % self.size;
        let mut y = (index / self.size) + 1;
        let tree_height = self.grid[index];
        while y < self.size {
            if self.grid[y * self.size + x] >= tree_height {
                return false;
            }
            y += 1;
        }
        true
    }

    /// Compute the visible distance to the top for a given
    /// index (corresponding to a tree). If the tree is on the
    /// top edge of the forest, it's equal to 0.
    fn view_distance_top(&self, index: usize) -> usize {
        if index < self.size {
            return 0;
        }

        let tree_x = index % self.size;
        let tree_y = index / self.size;
        let tree_h = self.grid[index];

        let mut curr_y = tree_y - 1;

        while curr_y > 0 {
            if tree_h <= self.grid[curr_y * self.size + tree_x] {
                return tree_y - curr_y;
            }
            curr_y -= 1;
        }
        if self.grid[tree_x] < tree_h {
            return tree_y - curr_y;
        }
        return tree_y - curr_y + 1;
    }

    /// Compute the visible distance to the bottom for a given
    /// index (corresponding to a tree). If the tree is on the
    /// bottom edge of the forest, it's equal to 0.
    fn view_distance_bottom(&self, index: usize) -> usize {
        if index >= self.size * (self.size - 1) {
            return 0;
        }

        let tree_x = index % self.size;
        let tree_y = index / self.size;
        let tree_h = self.grid[index];

        let mut curr_y = tree_y + 1;

        while curr_y < self.size - 1 {
            if tree_h <= self.grid[curr_y * self.size + tree_x] {
                return curr_y - tree_y;
            }
            curr_y += 1;
        }
        return curr_y - tree_y;
    }

    /// Compute the visible distance to the left for a
    /// a given index (corresponding to a tree). If the tree
    /// is on the left edge of the forest, it's equal to 0.
    fn view_distance_left(&self, index: usize) -> usize {
        if index % self.size == 0 {
            return 0;
        }

        let tree_x = index % self.size;
        let tree_y = index / self.size;
        let tree_h = self.grid[index];

        let mut curr_x = tree_x - 1;

        while curr_x > 0 {
            if tree_h <= self.grid[tree_y * self.size + curr_x] {
                return tree_x - curr_x;
            }
            curr_x -= 1;
        }
        return tree_x - curr_x;
    }

    /// Compute the visible distance to the right for a
    /// given index (corresponding to a tree). If the tree
    /// is on the right edge of the forest, it's equal to 0.
    fn view_distance_right(&self, index: usize) -> usize {
        if (index + 1) % self.size == 0 {
            return 0;
        }

        let tree_x = index % self.size;
        let tree_y = index / self.size;
        let tree_h = self.grid[index];

        let mut curr_x = tree_x + 1;

        while curr_x < self.size - 1 {
            if tree_h <= self.grid[tree_y * self.size + curr_x] {
                return curr_x - tree_x;
            }
            curr_x += 1;
        }
        return curr_x - tree_x;
    }

    /// Compute the scenic score for a given index (corresponding
    /// to a tree). The score corresponds to the product of its
    /// distance to the top, the bottom, the right and the left.
    fn scenic_score(&self, index: usize) -> usize {
        self.view_distance_top(index)
            * self.view_distance_bottom(index)
            * self.view_distance_left(index)
            * self.view_distance_right(index)
    }

    /// Return the highest scenic score found
    /// for a tree in the forest.
    fn highest_scenic_score(&self) -> usize {
        let mut max = 0;
        for i in 0..self.grid.len() {
            if self.scenic_score(i) > max {
                max = self.scenic_score(i);
            }
        }
        return max;
    }
}

impl From<std::fs::File> for TreePatch {
    /// Build a `TreePatch` object from the given input file.
    fn from(file: std::fs::File) -> Self {
        let reader = BufReader::new(file);

        let mut grid: Vec<usize> = vec![];
        let mut size = 0;

        for line in reader.lines() {
            match line {
                Ok(l) => {
                    if l.len() < 2 {
                        continue;
                    }
                    size = l.len();
                    for char in l.chars() {
                        grid.push(char as usize - '0' as usize);
                    }
                }
                Err(_) => {
                    panic!("failed to read input line");
                }
            }
        }
        TreePatch::new(size, grid)
    }
}

fn main() {
    let input_file = File::open("input.txt").expect("failed to open input file");
    let tree_patch = TreePatch::from(input_file);

    solve_problem_1(&tree_patch);
    solve_problem_2(&tree_patch);
}

/// Display the solution for the first problem.
fn solve_problem_1(tree: &TreePatch) {
    println!("solution #1: {}", tree.visible_trees());
}

/// Display the solution for the second problem.
fn solve_problem_2(tree: &TreePatch) {
    println!("solution #2: {}", tree.highest_scenic_score());
}

#[cfg(test)]
mod tests {
    use super::*;

    fn mock_tree() -> TreePatch {
        let file = File::open("example_input.txt").unwrap();
        TreePatch::from(file)
    }

    #[test]
    fn test_is_visible_from_right() {
        let tree_patch = mock_tree();

        let mut visible_trees_from_right = 0;
        for index in 0..tree_patch.grid.len() {
            if tree_patch.is_visible_from_right(index) {
                visible_trees_from_right += 1;
            }
        }
        assert_eq!(visible_trees_from_right, 11);
    }

    #[test]
    fn test_is_visible_from_left() {
        let tree_patch = mock_tree();

        let mut visible_trees_from_left = 0;
        for index in 0..tree_patch.grid.len() {
            if tree_patch.is_visible_from_left(index) {
                visible_trees_from_left += 1;
            }
        }
        assert_eq!(visible_trees_from_left, 11);
    }

    #[test]
    fn test_is_visible_from_top() {
        let tree_patch = mock_tree();

        let mut visible_trees_from_top = 0;
        for index in 0..tree_patch.grid.len() {
            if tree_patch.is_visible_from_top(index) {
                visible_trees_from_top += 1;
            }
        }
        assert_eq!(visible_trees_from_top, 10);
    }

    #[test]
    fn test_is_visible_from_bottom() {
        let tree_patch = mock_tree();

        let mut visible_trees_from_bottom = 0;
        for index in 0..tree_patch.grid.len() {
            if tree_patch.is_visible_from_bottom(index) {
                visible_trees_from_bottom += 1;
            }
        }
        assert_eq!(visible_trees_from_bottom, 8);
    }

    #[test]
    fn test_highest_scenic_tree() {
        let tree_patch = mock_tree();
        assert_eq!(tree_patch.highest_scenic_score(), 8);
    }

    #[test]
    fn test_scenic_score() {
        let tree_patch = mock_tree();
        assert_eq!(tree_patch.scenic_score(7), 4);
        assert_eq!(tree_patch.scenic_score(17), 8);
    }

    #[test]
    fn test_view_distance_top() {
        let tree_patch = mock_tree();
        assert_eq!(tree_patch.view_distance_top(0), 0);
        assert_eq!(tree_patch.view_distance_top(4), 0);
        assert_eq!(tree_patch.view_distance_top(7), 1);
        assert_eq!(tree_patch.view_distance_top(17), 2);
    }

    #[test]
    fn test_view_distance_bottom() {
        let tree_patch = mock_tree();
        assert_eq!(tree_patch.view_distance_bottom(20), 0);
        assert_eq!(tree_patch.view_distance_bottom(24), 0);
        assert_eq!(tree_patch.view_distance_bottom(17), 1);
        assert_eq!(tree_patch.view_distance_bottom(3), 4);
    }

    #[test]
    fn test_view_distance_left() {
        let tree_patch = mock_tree();
        assert_eq!(tree_patch.view_distance_left(0), 0);
        assert_eq!(tree_patch.view_distance_left(20), 0);
        assert_eq!(tree_patch.view_distance_left(1), 1);
        assert_eq!(tree_patch.view_distance_left(6), 1);
        assert_eq!(tree_patch.view_distance_left(7), 1);
        assert_eq!(tree_patch.view_distance_left(19), 4);
        assert_eq!(tree_patch.view_distance_left(3), 3);
    }

    #[test]
    fn test_view_distance_right() {
        let tree_patch = mock_tree();
        assert_eq!(tree_patch.view_distance_right(4), 0);
        assert_eq!(tree_patch.view_distance_right(24), 0);
        assert_eq!(tree_patch.view_distance_right(21), 2);
        assert_eq!(tree_patch.view_distance_right(3), 1);
        assert_eq!(tree_patch.view_distance_right(8), 1);
        assert_eq!(tree_patch.view_distance_right(10), 4);
    }
}
